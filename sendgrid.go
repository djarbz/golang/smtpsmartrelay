package main

import (
	"encoding/base64"
	"fmt"
	"log"
	"os"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type sendGridServer struct {
	ApiKey              string
	ApiHost             string
	ApiEndpoint         string
	FromNameOverride    string
	FromAddressOverride string
	// TODO: Configure Rate Limiting https://pkg.go.dev/golang.org/x/time/rate
}

func (s sendGridServer) OverrideName(name string) string {
	if s.FromNameOverride == "" {
		s.FromNameOverride = os.Getenv("SENDGRID_FROM_NAME_OVERRIDE")
	}
	if s.FromNameOverride == "" {
		return name
	}
	return s.FromNameOverride
}

func (s sendGridServer) OverrideAddress(address string) string {
	if s.FromAddressOverride == "" {
		s.FromAddressOverride = os.Getenv("SENDGRID_FROM_ADDRESS_OVERRIDE")
	}
	if s.FromAddressOverride == "" {
		return address
	}
	return s.FromAddressOverride
}

func (s sendGridServer) GetKey() string {
	if s.ApiKey == "" {
		s.ApiKey = os.Getenv("SENDGRID_API_KEY")
	}
	return s.ApiKey
}

func (s sendGridServer) GetHost() string {
	if s.ApiHost == "" {
		s.ApiHost = os.Getenv("SENDGRID_API_HOST")
	}
	if s.ApiHost == "" {
		s.ApiHost = "https://api.sendgrid.com"
	}
	return s.ApiHost
}

func (s sendGridServer) GetEndpoint() string {
	if s.ApiEndpoint == "" {
		s.ApiEndpoint = os.Getenv("SENDGRID_API_ENDPOINT")
	}
	if s.ApiEndpoint == "" {
		s.ApiEndpoint = "/v3/mail/send"
	}
	return s.ApiEndpoint
}

func (s sendGridServer) Send(m message) error {
	message := mail.NewV3Mail()
	message.SetFrom(mail.NewEmail(s.OverrideName(m.From.Name), s.OverrideAddress(m.From.Address)))
	message.Subject = m.GetSubject()
	personalize := mail.NewPersonalization()
	for _, addr := range m.TO {
		personalize.AddTos(mail.NewEmail(addr.Name, addr.Address))
	}
	for _, addr := range m.CC {
		personalize.AddCCs(mail.NewEmail(addr.Name, addr.Address))
	}
	for _, addr := range m.BCC {
		personalize.AddBCCs(mail.NewEmail(addr.Name, addr.Address))
	}
	message.AddPersonalizations(personalize)
	if m.Envelope.Text != "" {
		message.AddContent(mail.NewContent("text/plain", m.Envelope.Text))
	}
	if m.Envelope.HTML != "" {
		message.AddContent(mail.NewContent("text/html", m.Envelope.HTML))
	}
	if m.Envelope.Inlines != nil {
		for _, attachment := range m.Envelope.Inlines {
			sgAttachment := mail.NewAttachment()
			sgAttachment.Filename = attachment.FileName
			sgAttachment.Type = attachment.ContentType
			sgAttachment.Disposition = attachment.Disposition
			sgAttachment.Content = base64.StdEncoding.EncodeToString(attachment.Content)
			message.AddAttachment(sgAttachment)
		}
	}
	if m.Envelope.Attachments != nil {
		for _, attachment := range m.Envelope.Attachments {
			sgAttachment := mail.NewAttachment()
			sgAttachment.Filename = attachment.FileName
			sgAttachment.Type = attachment.ContentType
			sgAttachment.Disposition = attachment.Disposition
			sgAttachment.Content = base64.StdEncoding.EncodeToString(attachment.Content)
			message.AddAttachment(sgAttachment)
		}
	}

	request := sendgrid.GetRequest(s.GetKey(), s.GetEndpoint(), s.GetHost())
	request.Method = "POST"
	request.Body = mail.GetRequestBody(message)
	response, err := sendgrid.API(request)
	if err != nil {
		log.Println(err)
	} else {
		fmt.Println(response.StatusCode)
		fmt.Println(response.Body)
		fmt.Println(response.Headers)
	}
	return nil
}
