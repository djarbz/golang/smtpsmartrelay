package main

import (
	"log"
	"os"
	"time"

	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
)

func main() {
	// TODO: Load Configuration

	be := &backend{}
	be.sender = new(sendGridServer)

	s := smtp.NewServer(be)

	s.Addr = ":" + getenv("SMTP_SMART_RELAY_PORT", "2525")
	s.Domain = getenv("SMTP_SMART_RELAY_DOMAIN", "localhost")
	s.ReadTimeout = 10 * time.Second
	s.WriteTimeout = 10 * time.Second
	s.MaxMessageBytes = 1024 * 1024
	s.MaxRecipients = 50
	s.AllowInsecureAuth = true
	// sasl.Login is depreciated and is only enabled for backwards compatibility.
	// https://github.com/emersion/go-smtp/issues/41
	s.EnableAuth(sasl.Login, func(conn *smtp.Conn) sasl.Server {
		return sasl.NewLoginServer(func(username, password string) error {
			state := conn.State()
			session, err := be.Login(&state, username, password)
			if err != nil {
				return err
			}

			conn.SetSession(session)
			return nil
		})
	})

	log.Println("Starting server at", s.Addr)
	if err := s.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}
