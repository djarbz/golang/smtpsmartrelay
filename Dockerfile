FROM scratch
COPY smtpSmartRelay /usr/bin/smtpSmartRelay
ENTRYPOINT ["/usr/bin/smtpSmartRelay"]
